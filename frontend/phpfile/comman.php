<?php


	function global_message($status, $code, $data=false)
	{
		$msg =global_message_code($code);
		if($status==200){
			$result = array('status'=>$status,'msg'=> $msg,'code'=>$code,'data' => $data);
			
		}else{
			$result = array('status'=>$status,'code'=>$code,'error' => $msg, 'data' => $data);
			
		}
		
		return $result;
	}
	
	 function global_message_code($code)
	{
		$messageArray = array(
			200 => 'success',
			
			// Admin messages.
			1001=>'Data Insert SuccessFUlly',
			1002=>'already Exist',
			1003=>'Invalid credential. Please try with another.',
			1004=>'LogIn SuccessFully',
			1005=>'No User Found',
			1006=>'No Data Found',
			1007=>'Data available',
			1008=>'Last Inserted Id',
			1009=>'Update SuccessFully',
			1010=>'Delete SuccessFully',
			1011=>'City Add SuccesFully',
			1012=>'Data Not Inserted SuccessFully',
			1013=>'City And State Found',
			1014=>'State and City Not Found',
			1015=>'Api Key Insert SuccessFully',
			1016=>'Api Key Not available',
			1017=>'Api Key found',
			1018=>'Zone Add SuccesFully',
			1019=>'User Profile Update',
			
			// common for all when function not found anywhere in controller.
			404 => 'The server has not found anything matching the URI given'
		);
		if(isset($code) && !empty($code)){
			return $messageArray[$code];
		}
		return array();
	}

	

?>