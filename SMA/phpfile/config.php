<?php
define("WP_MEMORY_LIMIT", "1024M");

  /*****************************************************************
  Method:             connection()
  InputParameter:     
  Return:             connection
  *****************************************************************/
  function connection() 
  {
    ini_set('memory_limit', '1024M');
    ini_set('allow_url_fopen', '1');
    $conn = mysql_connect('localhost', 'mylimo6_airport', 'mylimo6@1234') or die(mysql_error());

    mysql_select_db('mylimo6_limo', $conn);

    return $conn;

  }

  /*****************************************************************
  Method:             operations()
  InputParameter:     query
  Return:             operations
  *****************************************************************/
  function operations($query) 
  {
    if ($query != null) 
    {
        $conn = connection();
        $res = mysql_query($query) or die(mysql_error());
        if (@mysql_num_rows($res) > 0) 
        {
          $arr = array();
          while ($row = mysql_fetch_assoc($res)) 
          {
              $arr[] = $row;
          }
          return $arr;

    } 
    else 
    {
      return TRUE;
    }

    }
  }
  
?>