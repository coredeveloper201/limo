<?php 

include_once 'config.php';
include_once 'comman.php';
define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
Method:             getRateMatrix()
InputParameter:    	user_id
Return:             get Rate Matrix
*****************************************************************/
	function getRateMatrix()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	  	{
	  		$query1="SELECT id,name from mileage_rate_matrix where user_id='".$_REQUEST['user_id']."'";
		 	$resource1 = operations($query1);
		  	for($j=0; $j<count($resource1); $j++)
			{	
				$arr[] = array(
	                "id" => $resource1[$j]['id'],
	                "name" => $resource1[$j]['name']
	            );			
			}  
		   	if(count($arr)>0 && gettype($arr)!="boolean")
		   	{
			   $result=global_message(200,1007,$arr);
		   	}
		   	else
		   	{
			   	$result=global_message(200,1006);
		   	}	  
	  	}
	  	else
	  	{
	  		$result=global_message(201,1003);
	  	}
		return  $result;
	}

/*****************************************************************
Method:             setDriverGratuity()
InputParameter:    	vehicle_code, sma_id, user_id
Return:             set Driver Gratuity
*****************************************************************/
	function setDriverGratuity()
	{	
	 	if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) &&(isset($_REQUEST['value']) && !empty($_REQUEST['value'])))
	   	{
	      	$userId=$_REQUEST['user_id'];
	  	   	$VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   	$service_typeArray=explode(',',$_REQUEST['service_typeArray']);
			$addSma=explode(',',$_REQUEST['sma_id']);
		   	$value=($_REQUEST['value']);
		   	$query ="insert into driver_gratuity(value,user_id) value('".$_REQUEST['value']."','".$userId."')";  
			$resource = operations($query);
			$dg_id=mysql_insert_id();

		  	for($i=0;$i<count($VehicleCode);$i++)
		  	{
			  	$Vehquery="insert into dg_vehicle(dg_id,vehicle_code,user_id) value('".$dg_id."','".$VehicleCode[$i]."','".$userId."')";	
			  	$resource1 = operations($Vehquery);
		  	}
			
			for($i=0;$i<count($service_typeArray);$i++)
		  	{
		  		$Vehquery="insert into drive_gratuity_service(parent_id,service_type,user_id) value('".$dg_id."','".$service_typeArray[$i]."','".$userId."')";	
			  	$resource1 = operations($Vehquery);
		  	}
			
			for($j=0;$j<count($addSma);$j++)
		  	{
				$Smaquery="insert into dg_sma(dg_id,sma_id,user_id) value('".$dg_id."','".$addSma[$j]."','".$userId."')";	
			  	$resource2 = operations($Smaquery);
		 	}
			$result=global_message(200,1008,$dg_id);		   
	   	}
	   	else
	   	{
	    	$result=global_message(201,1003);
		}	
		return $result;	
	}

	/*****************************************************************
	Method:             getRateMatrixList()
	InputParameter:    	user_id
	Return:             get Rate Matrix List
	*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
  		{
			$query="Select * from driver_gratuity where user_id='".$_REQUEST['user_id']."' order by  value";
			$resource= operations($query);
			$contents = array();
			for($i=0; $i<count($resource); $i++)
			{
				if(count($resource)>0 && gettype($resource)!="boolean")
		   		{
					$vehicle_code=''; 
					$sma_name='';
					$sma_id='';
					$value='';
					$Vehquery="Select vehicle_code from dg_vehicle where dg_id=".$resource[$i]['id'];
					$resource1= operations($Vehquery);
					for($j=0; $j<count($resource1); $j++)
					{
						$vehicle_code .=$resource1[$j]['vehicle_code'].',';
					}
					$driver_gratuity_query="Select service_type ,b.service_name from drive_gratuity_service a inner join comman_service_type b on a.service_type=b.service_code  where a.parent_id=".$resource[$i]['id']." and b.user_id='".$_REQUEST['user_id']."'";
					$driver_gratuity_query_result= operations($driver_gratuity_query);
					$driver_gratuity_query_result= operations($driver_gratuity_query);
					$service_typeArrayDublicate=[];
					$service_type='';
					$service_name='';
					for($j=0; $j<count($driver_gratuity_query_result); $j++)
					{
						if(!in_array($driver_gratuity_query_result[$j]['service_type'],$service_typeArrayDublicate))
						{
							array_push($service_typeArrayDublicate,$driver_gratuity_query_result[$j]['service_type']);
							$service_type .=$driver_gratuity_query_result[$j]['service_type'].',';
							$service_name .=$driver_gratuity_query_result[$j]['service_name'].',';
						}
					}
					$Smaquery="Select sma_id,sma_name from dg_sma,sma where sma.id=dg_sma.sma_id AND dg_sma.dg_id=".$resource[$i]['id'];
					$resource2= operations($Smaquery);
					for($k=0; $k<count($resource2); $k++)
					{
						$sma_name .=$resource2[$k]['sma_name'].',';
						$sma_id .=$resource2[$k]['sma_id'].',';
					}
					$contents[$i]['id']=$resource[$i]['id'];
					$contents[$i]['value']=$resource[$i]['value'];
					$contents[$i]['sma_id'] = $sma_id;
					$contents[$i]['service_type'] = $service_type;
					$contents[$i]['service_name'] = $service_name;
					$contents[$i]['sma_name'] = $sma_name;
					$contents[$i]['vehicle_code']=$vehicle_code;
				}
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
	   		{
		   		$result=global_message(200,1007,$contents);
			}
	   		else
	   		{
		   		$result=global_message(200,1006);
		   	}		  
		}
 		else
  		{
	  		$result=global_message(201,1003);
  		}
  		return  $result;
	}

	/*****************************************************************
	Method:             editRateMatrix()
	InputParameter:    	user_id
	Return:             edit Rate Matrix
	*****************************************************************/
	function editRateMatrix()
	{
		if((isset($_REQUEST['rate_matrix_id']) && !empty($_REQUEST['rate_matrix_id'])))
  		{
			$query="Select * from rate_calculate_mileage where mileage_matrix_id=".$_REQUEST['rate_matrix_id'];
			$resource= operations($query);
			$contents = array();

			for($i=0; $i<count($resource); $i++)
			{
				$contents[$i]['miles']=$resource[$i]['to_mileage'];
				$contents[$i]['fixed_amount']=$resource[$i]['fixed_amount'];
				$contents[$i]['rate'] = $resource[$i]['rate'];
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
			   $result=global_message(200,1007,$contents);
   		   	}
		   	else
		   	{
		   		$result=global_message(200,1006);
		   	}		  
		}
 		else
  		{
	  		$result=global_message(201,1003);
  		}
  		return  $result;	
	}

	/*****************************************************************
	Method:             deleteDriverGratuity()
	InputParameter:    	dg_id
	Return:             delete Driver Gratuity
	*****************************************************************/
	function deleteDriverGratuity()
	{
	 	if((isset($_REQUEST['dg_id']) && !empty($_REQUEST['dg_id'])))
	   	{
		  	$rowId=$_REQUEST['dg_id'];			 
			$query="delete from driver_gratuity where id='".$rowId."'";
		    $resource = operations($query);
			$queryDelete1="delete  from dg_sma where dg_id='".$rowId."'";
			$resource2 = operations($queryDelete1);
			$queryDelete2="delete  from dg_vehicle where dg_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$queryDelete5="delete  from drive_gratuity_service where parent_id='".$rowId."'";
			$resource5 = operations($queryDelete5);
			$result=global_message(200,1010);   
		}
	  	else
	  	{
		   $result=global_message(201,1003);
	  	}
		return $result;
	}

	/*****************************************************************
	Method:             checkUniqueMatrix()
	InputParameter:    	matrix_name
	Return:             check Unique Matrix
	*****************************************************************/
	function checkUniqueMatrix()
	{
		$query1="Select id from mileage_rate_matrix where name='".$_REQUEST['matrix_name']."' AND user_id='".$_REQUEST['user_id']."'";
			$resource1 = operations($query1);
		for($j=0; $j<count($resource1); $j++)
		{	
			$arr[] = array(
                "id" => $resource1[$j]['id']
            );			
		}  
		if(count($arr)>0 && gettype($arr)!="boolean")
	   	{
		   $result=global_message(200,1007,$arr);			   
	   	}
	   	else
	 	{
		   $result=global_message(200,1006);
	   	}	
	  	return  $result;
	}