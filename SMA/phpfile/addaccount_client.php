<?php

include_once 'addaccount_server.php';

$action = $_REQUEST['action'];
$response=array();
switch ($action) {
	case "addAccount":
		$response=addAccount();
		echo json_encode($response);
		break;

    case "GetAccountAddresses":
		$response=GetAccountAddresses();
		echo json_encode($response);
		break;

    case "GetAccountCreditCards":
		$response=GetAccountCreditCards();
		echo json_encode($response);
	    break;	

	case "GetAirlines":
		$response=GetAirlines();
		echo json_encode($response);
	    break;
	    
    case "GetCruiseShipsWithLines":
		$response=GetCruiseShipsWithLines();
		echo json_encode($response);
	    break;         	

	case "createAccount":
		$response=createAccount();
		echo json_encode($response);
		break;	
      
    case "getuserAccounList":
		$response=getuserAccounList();
		echo json_encode($response);
		break;

	case "viewuserInfo":
		$response=viewuserInfo();
		echo json_encode($response);
		break;

	case "deleteUserInfo":
		$response=deleteUserInfo();
		echo json_encode($response);
		break;

	case "updateUserAccount":
		$response=updateUserAccount();
		echo json_encode($response);
		break;
}
?>