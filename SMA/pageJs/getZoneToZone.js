/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: zone to Zone
    Author: Mylimoproject
---------------------------------------------*/
/*create a class name for the zone */	

	var _SERVICEPATHServer="phpfile/client.php";
	var _SERVICEPATH="phpfile/service.php";
	//getStateOnOptionButton();
	citiesAllClickBtn();
	getState();	

	/*---------------------------------------------
       Function Name: getStateOnOptionButton()
       Input Parameter: 
       return:json data
    ---------------------------------------------*/
/*	function getStateOnOptionButton(){
		var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalueUserInformation)=="string"){
	   		getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
		}
		
		$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalue)=="string"){
   			getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}
		
		$.ajax({
			url: _SERVICEPATHServer,
			type: 'POST',
			data: "action=getCity&user_id="+getLocalStoragevalue[0].id,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}
				
				for (var i = 0; i < responseObj.data.length; i++){
					responseHTML+='<option seq='+responseObj.data[i].id+'>'+responseObj.data[i].city_name+'</option>';
				}

				$('#select_cities').html(responseHTML);

		    }
		});
	}
*/
	/*---------------------------------------------
       Function Name: citiesAllClickBtn()
       Input Parameter: 
       return:json data
    ---------------------------------------------*/   

	function citiesAllClickBtn(){
		$('#save_add_city_data').on("click",function(){
			if($('#save_add_city_data').html()=="UPDATE"){
				var sequence=$(this).attr("seq");
				var cityName=$('#cityName').val();
				var cityAbbr=$('#cityAbbr').val();
				var stateName=$('#select_cities').val();
				var stateCode = $('#city_code').val();
				var zipcode=$('#zipcode').val();			
				updateCitiesState(sequence,cityName,cityAbbr,stateName,stateCode,zipcode);
				$('#add_city_popup').hide();
			}
			else{
			  
				var cityName=$('#cityName').val();
				var cityAbbr=$('#cityAbbr').val();
				var stateName=$('#select_cities').val();
				var stateCode = $('#city_code').val();
				var zipcode=$('#zipcode').val();
				setZoneRate(cityName,cityAbbr,stateName,stateCode,zipcode);
				$('#add_city_popup').hide();
			}
		});

		$('.popup_edit_city').on("click",function(){
			$('#cityName').val("");
			$('#cityAbbr').val("");
			$('#zipcode').val("");
			$('#select_cities').val("");
			$('#city_code').val("");
			$('#save_add_city_data').html("SAVE");
			$('#add_city_popup').show();
		})

        $('#popup_close_addrate').on("click",function(){
		  	$('#add_city_popup').hide();
		})
	}	   

	/*---------------------------------------------
       Function Name: updateCitiesState()
       Input Parameter: sequence,cityName,cityAbbr,stateName,stateCode,zipcode
       return:json data
    ---------------------------------------------*/ 

	function updateCitiesState(sequence,cityName,cityAbbr,stateName,stateCode,zipcode){
		$.ajax({
			url :_SERVICEPATHServer,
			type : 'post',
			data :  'action=updateCitiesZone&rowId='+sequence+'&cityName='+cityName+'&cityAbbr='+cityAbbr+'&stateName='+stateName+'&stateCode='+stateCode+'&zipcode='+zipcode,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}

				$('#city_'+sequence).remove();
				var k=0;
				$('.indexNumber').each(function(index, element){
					k++;
				});

			 	$('#cityState').append('<tr id="city_'+sequence+'"> <td  class="indexNumber" >'+k+'</td> <td id="cityName_'+sequence+'">'+cityName+'</td> <td id="cityAbbr_'+sequence+'">'+cityAbbr+'</td> <td id="cityStateName_'+sequence+'">'+stateName+'</td> <td id="zipcode_'+sequence+'">'+zipcode+'</td> <td> <div class="as" ng-show="!rowform.$visible"><a class="btn btn-xs popup_edit_city_state" city_code='+stateCode+' seq='+sequence+' >Edit</a> <a class="btn btn-xs delele_add_rate_row" seq='+sequence+' >Delete</a> </div> </td> </tr>');
				$('.popup_edit_city_state').on("click",function(){
					var sequence=$(this).attr("seq");
					var city_code=$(this).attr("city_code");
					$('#save_add_city_data').html("UPDATE");
					$('#save_add_city_data').attr("seq",sequence);
					$('#cityName').val($('#cityName_'+sequence).html());
					$('#cityAbbr').val($('#cityAbbr_'+sequence).html());
					$('#zipcode').val($('#zipcode_'+sequence).html());
					$('#select_cities').val($('#cityStateName_'+sequence).html());
					$('#city_code').val(city_code);
					$('#add_city_popup').show();
				});

				$('.delele_add_rate_row').on("click",function(){
					var sequence=$(this).attr("seq");		
					var rowId=sequence.split('_');
					$('#city_'+sequence).remove();
					deleteCities(rowId)	
				})
			}
		});
	}


	/*---------------------------------------------
       Function Name: getState()
       Input Parameter:
       return:json data
    ---------------------------------------------*/ 
	function getState(){
		var getUserInfo=window.localStorage.getItem("companyInfo");
		var getUserInfoObj=getUserInfo;
		if(typeof(getUserInfo)=="string"){
			getUserInfoObj=JSON.parse(getUserInfo);
		}

		$.ajax({
			url :_SERVICEPATHServer,
			type : 'post',
			data : 'action=getZoneState&user_id='+getUserInfoObj[0].id,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}
				
				for (var i = 0; i < responseObj.data.length; i++) {
					responseHTML+='<tr id="city_'+responseObj.data[i].id+'"> <td  class="indexNumber">'+(i+1)+'</td> <td id="cityName_'+responseObj.data[i].id+'">'+responseObj.data[i].zone_name+'</td> <td id="cityAbbr_'+responseObj.data[i].id+'">'+responseObj.data[i].zone_abbr+'</td> <td id="cityStateName_'+responseObj.data[i].id+'">'+responseObj.data[i].city_name+'</td> <td id="zipcode_'+responseObj.data[i].id+'">'+responseObj.data[i].zip_code+'</td> <td> <div class="as" ng-show="!rowform.$visible"><a class="btn btn-xs popup_edit_city_state" seq='+responseObj.data[i].id+' city_code='+responseObj.data[i].city_code+'>Edit</a> <a class="btn btn-xs delele_add_rate_row" seq='+responseObj.data[i].id+' >Delete</a> </div> </td> </tr>';
				}

				$('#cityState').html(responseHTML);
				$('.popup_edit_city_state').on("click",function(){
					var sequence=$(this).attr("seq");
					var city_code=$(this).attr("city_code");
					$('#save_add_city_data').attr("seq",sequence);
					$('#save_add_city_data').html("UPDATE");
					$('#cityName').val($('#cityName_'+sequence).html());
					$('#cityAbbr').val($('#cityAbbr_'+sequence).html())
					$('#zipcode').val($('#zipcode_'+sequence).html())
					$('#select_cities').val($('#cityStateName_'+sequence).html());
					$('#city_code').val(city_code);
					$('#add_city_popup').show();
				});

				$('.delele_add_rate_row').on("click",function(){
					var sequence=$(this).attr("seq");
					var rowId=sequence.split('_');
					$('#city_'+sequence).remove();
					deleteCities(rowId)
				})

	      	}
	      });
		}
		
	/*---------------------------------------------
       Function Name: setZoneRate()
       Input Parameter:cityName,cityAbbr,stateName,stateCode,zipcode
       return:json data
    ---------------------------------------------*/ 
	function setZoneRate(cityName,cityAbbr,stateName,stateCode,zipcode){
		var getUserInfo=window.localStorage.getItem("companyInfo");
		var getUserInfoObj=getUserInfo;
		if(typeof(getUserInfo)=="string"){
			getUserInfoObj=JSON.parse(getUserInfo);
		}

		$.ajax({
			url :_SERVICEPATHServer,
			type : 'post',
			data : 'action=setZoneRate&user_id='+getUserInfoObj[0].id+'&cityName='+cityName+'&cityAbbr='+cityAbbr+'&stateName='+stateName+'&stateCode='+stateCode+'&zipcode='+zipcode,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}
				var k=0;
			    $('.indexNumber').each(function(index, element){
                    k++;
                });

				$('#cityState').append('<tr id="city_'+responseObj.data+'"> <td  class="indexNumber">'+(k+1)+'</td> <td id="cityName_'+responseObj.data+'">'+cityName+'</td> <td id="cityAbbr_'+responseObj.data+'">'+cityAbbr+'</td> <td id="cityStateName_'+responseObj.data+'">'+stateName+'</td> <td id="zipcode_'+responseObj.data+'">'+zipcode+'</td> <td> <div class="as" ng-show="!rowform.$visible"><a class="btn btn-xs popup_edit_city_state" seq='+responseObj.data+'>Edit</a> <a class="btn btn-xs delele_add_rate_row" seq='+responseObj.data+' >Delete</a> </div> </td> </tr>');
				$('.popup_edit_city_state').on("click",function(){
					var sequence=$(this).attr("seq");
					$('#save_add_city_data').attr("seq",sequence);
					$('#save_add_city_data').html("UPDATE");
					$('#cityName').val($('#cityName_'+sequence).html());
					$('#cityAbbr').val($('#cityAbbr_'+sequence).html())
					$('#zipcode').val($('#zipcode_'+sequence).html())
					$('#select_cities').val($('#cityStateName_'+sequence).html());
					$('#add_city_popup').show();
				});

				$('.delele_add_rate_row').on("click",function(){
					var sequence=$(this).attr("seq");
					var rowId=sequence.split('_');
					$('#city_'+sequence).remove();
					deleteCities(rowId)
				})
			}
		});
}

	/*---------------------------------------------
	   Function Name: deleteCities()
	   Input Parameter:rowId
	   return:json data
	---------------------------------------------*/ 
	function deleteCities(rowId){
		var getUserInfo=window.localStorage.getItem("companyInfo");
		var getUserInfoObj=getUserInfo;
		if(typeof(getUserInfo)=="string"){
			getUserInfoObj=JSON.parse(getUserInfo);
		}

		$.ajax({
			url: _SERVICEPATHServer,
			type: 'POST',
			data: 'action=deleteZone&user_id='+getUserInfoObj[0].id+'&id='+rowId,
			success: function(response){
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);
				}
			}
		});
	}

	



