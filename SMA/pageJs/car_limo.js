/***********************************************************
  * Method Name   : categorySelectOption
  * Description       : Retrieves User Selected option according by Location and category    
  * @Param            : cordinates,type, date,portfolio_id,category 
  * @return            : employee data json
  ***********************************************************/

onclick();
onclickMilage();
function onclick()
{
	/*###################################### code to hide and show data in add rate in rate setup page#####################################*/
	      $(".set > a").on("click", function(){
            if($(this).hasClass('active')){
                $(this).removeClass("active");
                $(this).siblings('.acc-content').slideUp("slow");
				$('.show_data').hide();
				  $('.show_data_airport').hide();
				    $('.show_data_point').hide();
                $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
            }else{
                $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
                $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
                $(".set > a").removeClass("active");
                $(this).addClass("active");
                $('.acc-content').slideUp("slow");
                $(this).siblings('.acc-content').slideDown("slow");
            }

        });
			/*###################################### code to hide and show data in add rate in rate setup page#####################################*/
		
		    $(".show_details1").on("click", function(){
   
   if($(this).hasClass('active')){
                $(this).removeClass("active");
               $('.show_data').slideUp("slow");
           $(".change_icon i").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
            }
   
   
   else
   {
   
     $(this).addClass("active");
      $('.show_data').slideDown("slow");;
    $(".change_icon i").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
    
   }
    });
		  	/*###################################### code to hide and show popup in add rate in rate setup page#####################################*/
		   $('.popup_edit1').off();
		   $('.popup_edit1').on("click",function(){
			   $('#save_data1').html("Save");
				$('#signup_form_overlay').show();
				$('#add_hours').val("");
				$('#source').val("");
						$('#add_rate').val("");
			  });
			  $('.popup_edit').off();
			  $('.popup_edit').on("click",function(){
			   $('#save_data1').html("Save");
				$('#signup_form_overlay').show();
				
			  });
			  /*######################### delete data of row in add rates####################################*/
			 
		
			   $('#popup_close_vrc').on("click",function(){
		  $('#signup_form_overlay').hide();
		  });
		 
		  
		  	/*###################################### code to become input time hourly in rate setup page#####################################*/
 $('.input_box').on("click",function(){
	   if($(this).hasClass('active')){
                $(this).removeClass("active");
				var getTextBoxValue=$('.input_box_to_edit input').val();
		   $('.input_box_to_edit').html(getTextBoxValue);
		/*   $('.input_box_update').hide();
		   $('.input_box').show();
				*/
		  }
			else
			{
				 $(this).addClass("active");
				$('.input_box_to_edit').html("<input type='text' value='"+ $('.input_box_to_edit').html() +"'>");
				/*$('.input_box').hide();
				$('.input_box_update').show();*/
		}
		  });
		  	/*###################################### code topopup data become dynamic in add rate in rate setup page#####################################*/
		  $('#save_data1').off();
$('#save_data1').on("click",function(){
	
	var count=0;
	$('.ratesetup_addrate').each(function(index, element) {
        count++;
    });
	
	if($('#save_data1').html()=="Update")
	{
		var getSequence=$(this).attr("seq");
	   $('#add_hour_'+getSequence).html($('#add_hours').val());
	    $('#add_rate_'+getSequence).html($('#add_rate').val());
		$('#select_service_'+getSequence).html($('#source').val());
	}
	else
	{
	 $('#add_rates').append('<tr class="ratesetup_addrate" id="'+count+'"><td><span>Up to </span><strong><span id="add_hour_'+count+'">'+$('#add_hours').val()+'</span></strong> hours</td> <td> <span id="select_service_'+count+'">'+$('#source').val()+'</span> </td> <td><strong class="ng-binding">$ <span class="ng-scope ng-binding editable" id="add_rate_'+count+'">'+$('#add_rate').val()+'</span> </strong> per hour </td> <td><div class="as" ng-show="!rowform.$visible"><a class="btn btn-xs popup_edit " seq='+count+' >Edit</a> <a class="btn btn-xs delele_row"  seq='+count+'  >Delete</a> </div> </td></tr>');
		
	}
	
	$('#signup_form_overlay').hide();
	
	$('.popup_edit').off();
    $('.popup_edit').on("click",function(){
		var sequence=$(this).attr("seq");
		$('#add_hours').val($('#add_hour_'+sequence).html());
		$('#source').val($('#select_service_'+sequence).html());
		$('#add_rate').val($('#add_rate_'+sequence).html());
		$('#save_data1').html("Update");
		$('#save_data1').attr("seq",sequence);
		$('#signup_form_overlay').show();
	 });
	 
	 			  $('.delele_row').on("click",function(){
					var sequence=$(this).attr("seq");
					$('#'+sequence).remove();
		
		
		
	 			});
	 
	 
	 
	 
	})
	 $("#add_hours").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ) {
        //display error message
        $("#errmsg1").html("Digits Only").show().fadeOut("slow");
               return false;
    }
	 });
	  $("#add_rate").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ) {
        //display error message
        $("#errmsg2").html("Digits Only").show().fadeOut("slow");
               return false;
    }
	 });
	
}
/** code of second page */
function onclickMilage(){
 $('.popup_edit1_milege').off();
		   $('.popup_edit1_milege').on("click",function(){
			   $('#save_data1_milege').html("Save");
				$('#signup_form_overlay_milege').show();
				$('#add_miles').val("");
				$('#Miles').val("");
						$('#add_rate_mile').val("");
			  });
			  
			  
			  
			   $('.popup_edit_milage').off();
			  $('.popup_edit_milage').on("click",function(){
			   $('#save_data1_milege').html("Update");
				$('#signup_form_overlay_milege').show();
				
			  });
			   $('#popup_close_vrc_milege').on("click",function(){
		  $('#signup_form_overlay_milege').hide();
		  })
		  /**  #############################code #####################################   **/
		   $('#save_data1_milege').off();
$('#save_data1_milege').on("click",function(){
	
	var count=0;

	$('.ratesetup_addrate_milage').each(function(index, element) {
        count++;
    });
	
	if($('#save_data1_milege').html()=="Update")
	{
		var getSequence=$(this).attr("seq");
		
	   $('#add_miles'+getSequence).html($('#add_miles').val());
	    $('#Rates'+getSequence).html($('#Rates').val());
	}
	else
	{

	 $('#add_rates_distance').append('<tr class="ratesetup_addrate_milage" id="'+count+'"> <td> <span >First</span> <strong> <span id="add_miles_'+count+'" > <input type="text" size="3" value='+$('#add_miles').val()+'> </span> </strong>miles</td> <td><strong class="ng-binding">$ <span class="ng-scope ng-binding editable"  id="Rates_'+count+'"> <input type="text" size="3" value='+$('#add_rate_mile').val()+'> </span> </strong> per mile </td> <td> <div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs popup_edit_milage " seq='+count+' >Edit</a> <a class="btn btn-xs delete_milage_btn"  seq='+count+' >Delete</a> </div> </td> </tr>');
	}
	
	
	$('#signup_form_overlay_milege').hide();


	$('.delete_milage_btn').on("click",function(){
		
	var getsequence=$(this).attr("seq");
	$('#'+getsequence).remove();	
		
	});



	$('.popup_edit_milage').off();
    $('.popup_edit_milage').on("click",function(){
		var sequence=$(this).attr("seq");
		
		$('#add_rate_mile').val($('#Rates_'+sequence+' input').val());
		
		$('#add_miles').val($('#add_miles_'+sequence +' input').val());
		$('#save_data1_milege').html("Update");
		$('#save_data1_milege').attr("seq",sequence);
		$('#signup_form_overlay_milege').show();
	 });
	})
	 $("#add_miles").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ) {
        //display error message
        $("#errmsg1").html("Digits Only").show().fadeOut("slow");
               return false;
    }
	 });
	  $("#add_rate_mile").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ) {
        //display error message
        $("#errmsg2").html("Digits Only").show().fadeOut("slow");
               return false;
    }
	 });
}
/***################################################################Additional rate and fees##############################################################**/

$('#add_fixed_rate').on("click",function(){
	$('#add_fix_rate').val("");
	$('#amount_fix_rate').val("");
	$('#apply_vihecle_fix_rate').val("");
	$('#apply_service_fix_rate').val("");
	$('#signup_form_overlay_fixed_rate').show();
	
	$('#save_data1_fix_rate').html("Save");
	$('#save_data1_fix_rate').off();
	$('#save_data1_fix_rate').on("click",function(){
		
		
		var count=0;

	$('.ratesetup_addrate').each(function(index, element) {
        count++;
    });
		if($('#save_data1_fix_rate').html()=="UPDATE")
		{
			var sequence=$(this).attr("seq");
			
			$('#fix_rate_Name_'+sequence).html($('#add_fix_rate').val());
			$('#fix_rate_amount_'+sequence).html($('#amount_fix_rate').val());
			$('#fix_rate_apply_'+sequence).html($('#apply_vihecle_fix_rate').val());
			$('#service_rate_apply_'+sequence).html($('#apply_service_fix_rate').val());
			$('#signup_form_overlay_fixed_rate').hide();
		}
		else
		{
			$('#ratesetup_addrate_fix_rate').append(' <tr class="ratesetup_addrate" id="fix_rate_'+count+'"> <td> <span id="fix_rate_Name_'+count+'">'+$('#add_fix_rate').val()+'</span> </td> <td> <span> Fixed Rate </span> </td> <td> <span id="fix_rate_amount_'+count+'"> $'+$("#amount_fix_rate").val()+'Is required </span> </td> <td style=" width: 21%; text-align: justify;" id="fix_rate_apply_'+count+'">'+$('#apply_vihecle_fix_rate').val()+'</td> <td style=" width: 16%; text-align: justify;" id="service_rate_apply_'+count+'">'+$('#apply_service_fix_rate').val()+'</td> <td> <div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs fix_rate_popup_edit "  seq="'+count+'">Edit</a> <a class="btn btn-xs fix_rate_popup_delete" seq='+count+'>Delete</a> </div> </td> </tr>');
			$('#signup_form_overlay_fixed_rate').hide();
	
		
		
		}
		$('.fix_rate_popup_edit').on("click",function(){
			$('#signup_form_overlay_fixed_rate').show();
			var sequence=$(this).attr("seq");
			
			var getFixRrateName=$('#fix_rate_Name_'+sequence).html();
			var getFixRrateAmount=$('#fix_rate_amount_'+sequence).html();
			var getFixRrateApply=$('#fix_rate_apply_'+sequence).html();
			$('#add_fix_rate').val(getFixRrateName);
			$('#amount_fix_rate').val(getFixRrateAmount);
			$('#apply_vihecle_fix_rate').val(getFixRrateApply);
			$('#save_data1_fix_rate').html('UPDATE');
			$('#save_data1_fix_rate').attr("seq",sequence);
			
			
			
		});
		
		$('.fix_rate_popup_delete').on("click",function(){
			
			var sequence=$(this).attr("seq");
			$("#fix_rate_"+sequence).remove();
			
			
		});
		
		
		
		
		
	});
	})
	
	$('#popup_close_vrc_fixed').on("click",function(){
		$('#signup_form_overlay_fixed_rate').hide();	
		
		})

/**end**/
/***################################################################incremental rate##############################################################**/
$('#incremental_rate').on("click",function(){
	$('#incremental_rate_name').val("");
	$('#incremental_rate_amount').val("");
	$('#incremental_rate_max_amount').val("");
	$('#service_incremental_rate').val("");
	$('#signup_form_overlay_incremental_rate').show();
	$('#save_data1_incremental_rate').html("Save");
	$('#save_data1_incremental_rate').off();
	$('#save_data1_incremental_rate').on("click",function(){
	var count=0;
	$('.ratesetup_addrate').each(function(index, element) {
        count++;
    });
		if($('#save_data1_incremental_rate').html()=="UPDATE")
		{
			
			var sequence=$(this).attr("seq");
			$('#incremental_rate_name_'+sequence).html($('#incremental_rate_name').val());
			$('#fix_rate_amount_'+sequence).html($('#incremental_rate_amount').val());
			$('#incremental_rate_max_amount_'+sequence).html($('#incremental_rate_max_amount').val());
			$('#incremental_service_amount_'+sequence).html($('#service_incremental_rate').val());
			
			
			$('#signup_form_overlay_incremental_rate').hide();
			
		}
		else
		{
		$('#ratesetup_addrate_fix_rate').append(' <tr class="ratesetup_addrate" id="incremental_rate_'+count+'"> <td> <span id="incremental_rate_name_'+count+'">'+$('#incremental_rate_name').val()+'</span> </td> <td> <span> Incremental Rate </span> </td> <td> <span id="incremental_rate_amount_'+count+'"> $'+$("#incremental_rate_amount").val()+'Is required </span> </td> <td style=" width: 21%; text-align: justify;" id="incremental_rate_max_amount_'+count+'">'+$('#incremental_rate_max_amount').val()+'</td> <td style=" width: 16%; text-align: justify;" id="incremental_service_amount_'+count+'">'+$('#service_incremental_rate').val()+'</td><td> <div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs incremental_popup_edit" seq="'+count+'">Edit</a> <a class="btn btn-xs incremental_popup_delete" seq="'+count+'" >Delete</a> </div> </td> </tr>');
		$('#signup_form_overlay_incremental_rate').hide();
		}
		
		
		$('.incremental_popup_edit').on("click",function(){
			$('#signup_form_overlay_incremental_rate').show();
			var sequence=$(this).attr("seq");
			
			var getFixRrateName=$('#incremental_rate_name_'+sequence).html();
			var getFixRrateAmount=$('#incremental_rate_amount_'+sequence).html();
			var getFixRrateApply=$('#incremental_rate_max_amount_'+sequence).html();
			var getFixRrateService=$('#incremental_service_amount_'+sequence).html();
			$('#service_incremental_rate').val(getFixRrateService);
			$('#incremental_rate_name').val(getFixRrateName);
			$('#incremental_rate_amount').val(getFixRrateAmount);
			$('#incremental_rate_max_amount').val(getFixRrateApply);
			$('#save_data1_incremental_rate').html('UPDATE');
			$('#save_data1_incremental_rate').attr("seq",sequence);
			
			
			
		});
		
		$('.incremental_popup_delete').on("click",function(){
			
			var sequence=$(this).attr("seq");
			$("#incremental_rate_"+sequence).remove();
			
			
		});
		
		
		
		
	});
	
	})
	
	$('#popup_close_vrc_incremental').on("click",function(){
		$('#signup_form_overlay_incremental_rate').hide();	
		
		})
		/**end**/
		/***################################################################base rate##############################################################**/
$('#base_rate').on("click",function(){
	$('#add_base_rate_name').val("");
	$('#add_base_rate_amount').val("");
	$('#add_base_rate_vehicle_name').val("");
	$('#add_base_rate_service_name').val("");
	$('#signup_form_overlay_base_rate').show();
	
	
	$('#add_base_rate_service_btn').on("click",function(){
				var count=0;

	$('.ratesetup_base_addrate').each(function(index, element) {
        count++;
    });
		
		
		
	if($('#add_base_rate_service_btn').html()=="UPDATE")
		{
			
			var sequence=$(this).attr("seq");
			$('#add_base_rate_name_'+sequence).html($('#add_base_rate_name').val());
			$('#add_base_rate_amount_'+sequence).html($('#add_base_rate_amount').val());
			$('#add_base_rate_vehicle_name_'+sequence).html($('#add_base_rate_vehicle_name').val());
			$('#add_base_rate_service_name_'+sequence).html($('#add_base_rate_service_name').val());
			$('#signup_form_overlay_base_rate').hide();
			
			
			
			
		}
		else
		{
		$('#ratesetup_addrate_fix_rate').append(' <tr class="ratesetup_base_addrate" id="base_rate_'+count+'"> <td> <span id="add_base_rate_name_'+count+'">'+$('#add_base_rate_name').val()+'</span> </td> <td> <span> Base Rate </span> </td> <td> <span id="add_base_rate_amount_'+count+'"> $'+$("#add_base_rate_amount").val()+'Is required </span> </td> <td style=" width: 21%; text-align: justify;" id="add_base_rate_vehicle_name_'+count+'">'+$('#add_base_rate_vehicle_name').val()+'</td> <td style=" width: 16%; text-align: justify;" id="add_base_rate_service_name_'+count+'">'+$('#add_base_rate_service_name').val()+'</td> <td> <div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs base_popup_edit" seq='+count+'>Edit</a> <a class="btn btn-xs base_popup_delete" seq='+count+'>Delete</a> </div> </td> </tr>');
		$('#signup_form_overlay_base_rate').hide();
		}
		
		$('.base_popup_edit').on("click",function(){
			
			
			$('#signup_form_overlay_base_rate').show();
			var sequence=$(this).attr("seq");
			var getFixRrateName=$('#add_base_rate_name_'+sequence).html();
			var getFixRrateAmount=$('#add_base_rate_amount_'+sequence).html();
			var getFixRrateApply=$('#add_base_rate_vehicle_name_'+sequence).html();
			var getFixRrateService=$('#add_base_rate_service_name_'+sequence).html();
			$('#add_base_rate_service_name').val(getFixRrateService);
			$('#add_base_rate_name').val(getFixRrateName);
			$('#add_base_rate_amount').val(getFixRrateAmount);
			$('#add_base_rate_vehicle_name').val(getFixRrateApply);
				$('#add_base_rate_service_name').val(getFixRrateService);
			$('#add_base_rate_service_btn').html('UPDATE');
			$('#add_base_rate_service_btn').attr('seq',sequence);
			
			
			
		});
		
		$('.base_popup_delete').on("click",function(){
			
			var sequence=$(this).attr("seq");
			$("#base_rate_"+sequence).remove();
			
			
		});
		
		
		
	});
	
	})
	
	$('#popup_close_vrc_base').on("click",function(){
		$('#signup_form_overlay_base_rate').hide();	
		
		})
		/**end**/
		/**########################################################## multi-select#############################################################################**/
$("#apply_vihecle_fix_rate").multiselect('destroy');
            $('#apply_vihecle_fix_rate').multiselect({
                maxHeight: 200,
                buttonWidth: '175px',
                includeSelectAllOption: true
            });
			$("#apply_service_fix_rate").multiselect('destroy');
            $('#apply_service_fix_rate').multiselect({
                maxHeight: 200,
                buttonWidth: '175px',
                includeSelectAllOption: true
            });
			$("#apply_incremental_rate").multiselect('destroy');
            $('#apply_incremental_rate').multiselect({
                maxHeight: 200,
                buttonWidth: '175px',
                includeSelectAllOption: true
            });
			$("#service_incremental_rate").multiselect('destroy');
            $('#service_incremental_rate').multiselect({
                maxHeight: 200,
                buttonWidth: '175px',
                includeSelectAllOption: true
            });
			$("#add_base_rate_vehicle_name").multiselect('destroy');
            $('#add_base_rate_vehicle_name').multiselect({
                maxHeight: 200,
                buttonWidth: '175px',
                includeSelectAllOption: true
            });
			$("#add_base_rate_service_name").multiselect('destroy');
            $('#add_base_rate_service_name').multiselect({
                maxHeight: 200,
                buttonWidth: '175px',
                includeSelectAllOption: true
            });
				/***################################################################additional add rate##############################################################**/
$('#popup_edit_additional').on("click",function(){
	$('#signup_form_overlay_additional_adddd').show();
	
	})
	
	$('#popup_edit_additional').on("click",function(){
		$('#signup_form_overlay_additional_adddd').hide();
			
		
		})
		/**end**/


$('#Miles_check').on("change",function(){
	
	if($("#Miles_check").is(':checked'))
    $("#next_mile").hide();  // checked
else
    $("#next_mile").show();

	});
