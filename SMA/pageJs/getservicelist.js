
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Service List
  Author: Mylimoproject
---------------------------------------------*/
/*set the php file path for webservice call*/
  var _SERVICEPATHServer="phpfile/service_type_client.php";

/*call the getservice function on page load*/
  getService();

/*Get the full name of user from loacalstorage*/
  var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalueUserInformation)=="string"){
	   	getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
	}
   $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);

/*---------------------------------------------
    Function Name: getService
    Input Parameter:user id 
    return:list of service type
 ---------------------------------------------*/ 
	function getService(){
        $('#refresh_overlay').css("display","block");
	    var getUserInfo=window.localStorage.getItem("companyInfo");
		var getUserInfoObj=getUserInfo;
		if(typeof(getUserInfo)=="string"){
		   getUserInfoObj=JSON.parse(getUserInfo);
		}
		$.ajax({
			url :_SERVICEPATHServer,
			type : 'post',
			data : 'action=getService&user_id='+getUserInfoObj[0].id,
	     	success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string"){
				   responseObj=JSON.parse(response);					
				}
			
				if(typeof(response)!=undefined){
					for (var i = 0; i < responseObj.data.length; i++) {
						if(responseObj.data[i].status=='1'){	
						    responseHTML+='<tr id="code_'+responseObj.data[i].id+'"> <td  class="indexNumber">'+(i+1)+'</td> <td id="serviceCode_'+responseObj.data[i].id+'">'+responseObj.data[i].service_code+'</td> <td id="serviceName_'+responseObj.data[i].id+'">'+responseObj.data[i].service_name+'</td> <td> <div class="as" ng-show="!rowform.$visible"><a class="btn btn-xs popup_edit_service" seq2="'+responseObj.data[i].service_name+'" seq="'+responseObj.data[i].id+'">Edit</a> <a class="btn btn-xs enable_service_type" seq='+responseObj.data[i].id+' >Disable</a> </div> </td> </tr>';
		                }else{
		                    responseHTML+='<tr id="code_'+responseObj.data[i].id+'"> <td  class="indexNumber">'+(i+1)+'</td> <td id="serviceCode_'+responseObj.data[i].id+'">'+responseObj.data[i].service_code+'</td> <td id="serviceName_'+responseObj.data[i].id+'">'+responseObj.data[i].service_name+'</td> <td> <div class="as" ng-show="!rowform.$visible"><a class="btn btn-xs popup_edit_service" seq2="'+responseObj.data[i].service_name+'" seq="'+responseObj.data[i].id+'">Edit</a> <a class="btn btn-xs disable_service_type" seq='+responseObj.data[i].id+' >Enable</a> </div> </td> </tr>';
		                } 
				             
					}
		        }else{
		            responseHTML+='<tr><td colspan="4">Data not Found.</td></tr>';
		        }
			    $('#service_type').html(responseHTML);
			    $('#refresh_overlay').css("display","none");
			    $('.popup_edit_service').on("click",function(){
                 
					var sequence=$(this).attr("seq");
					var sequence2=$(this).attr("seq2");
					$('#serviceName').val(sequence2);
					$('#update_service_name').attr("row_id",sequence);
					$('#update_service_popup').show();
				});

			    $('.enable_service_type').on("click",function(){
				    var row_id=$(this).attr("seq");
					var confirmValue=confirm("Are you sure,you want to Disable the service ");
		            if(confirmValue){
		               disableService(row_id);
		            }

				});

			    $('.disable_service_type').on("click",function(){

					var row_id=$(this).attr("seq");
					var confirmValue=confirm("Are you sure,you want to Enable the service ");
					if(confirmValue){
		               enableService(row_id);
				        
		            }

			    });

		      }
		});

    }

    /*click on popup close button */
    $('#popup_close_addrate').on("click",function(){
	    $('#update_service_popup').hide();
	});

    /*click on the the update of service type*/
    $('#update_service_name').on('click',function(){
	       
		var row_id=$(this).attr("row_id");
		var service_name=$('#serviceName').val();
	    var confirmValue=confirm("Are you sure,you want to Update the service ");
	    if(confirmValue){
	        updateCitiesState(row_id,service_name);
	    }
    });
     

   /*---------------------------------------------
      Function Name: getService
      Input Parameter:user id 
      return:list of service type
    ---------------------------------------------*/  
       
    function updateCitiesState(row_id,service_name){
        $('#refresh_overlay').css("display","block"); 
	    $.ajax({
			url :_SERVICEPATHServer,
			type : 'post',
			data :  'action=updateServiceName&rowId='+row_id+'&serviceName='+service_name,
	        success: function(response) {
				var responseHTML='';
				var responseObj=response;
                    responseObj=JSON.parse(response);
				if(responseObj.code==1002){
					alert('Service Name Updated Successfully');	
					$('#update_service_popup').hide();	
                    getService();
				}else{

                    alert('Service Name Not Updated');
				}
              $('#refresh_overlay').css("display","none");
			}
			
		});

	}

    /*---------------------------------------------
        Function Name: disableService
        Input Parameter:rowId
        return:list of service type
    ---------------------------------------------*/     
    function disableService(row_id){

	    $.ajax({
			url :_SERVICEPATHServer,
			type : 'post',
			data : 'action=disableService&rowId='+row_id,
	        success: function(response) {

				var responseHTML='';
				var responseObj=response;
                    responseObj=JSON.parse(response);
				if(responseObj.code==1002){
					 alert('Service Disable Successfully');	
                     getService();
				}else{
                    alert('Service  Not Disable');

				}

			}
			

	    });

    }

    /*---------------------------------------------
        Function Name: enableService
        Input Parameter:row_id
        return:list of service type
    ---------------------------------------------*/     
	function enableService(row_id){

	    $.ajax({
			url :_SERVICEPATHServer,
			type : 'post',
			data :  'action=enableService&rowId='+row_id,
	        success: function(response) {
				var responseHTML='';
				var responseObj=response;
                    responseObj=JSON.parse(response);
				if(responseObj.code==1002)
				{
					alert('Service Enable Successfully');	
                    getService();
				}else{

                    alert('Service Not Enable');

				}

		    }
			

	    });
	
    }