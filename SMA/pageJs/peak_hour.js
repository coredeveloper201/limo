
/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: Peack Hour page
    Author: Mylimoproject
---------------------------------------------*/
/*create a class for peack hour menu*/
var PeakHourMain={
	/*set the web service path using php file*/
	_Serverpath:"phpfile/sma_client.php",
/*---------------------------------------------
      Function Name: showPeakHourTimeDay()
      Input Parameter: getSeq
      return:json data
---------------------------------------------*/
	showPeakHourTimeDay:function(getSeq){
		$('#refresh_overlay').css("display","block");
		$.ajax({
			url: PeakHourMain._Serverpath,
			type: 'POST',
			dataType:'json',
		    data:"action=showPeakHourTimeDay&rowId="+getSeq,
		    success: function(response) {
				$('.mnd_check').each(function(index,result){
				var getSeqValue=parseInt($(this).val());
					$(this).prop("checked",false);
					$('.mndfrom'+getSeqValue).attr("disabled","true").val('');
					$('.mndto'+getSeqValue).attr("disabled","true").val('');
					$('.mndfrome'+getSeqValue).attr("disabled","true").val('');
					$('.mndtoe'+getSeqValue).attr("disabled","true").val('');
					
		        });

				if(response.code==1006)
				{
					var i=0;
					var getLastArray=[];
					$('.mnd_check').each(function(index,result){
						var getValue=$(this).val();
						var getInstanse=$(this);
						for(var j=0; j<response.data[0].dayInformation.length; j++)
						{
							if($('.dayName'+getValue).html().trim()==response.data[0]['dayInformation'][j]['day_name'])
							{
	                            var start_time=response.data[0]['dayInformation'][j]['start_time'];
	                            var start_time1=start_time.split(':');
	                                start_time1=start_time1[0]+':'+start_time1[1];

	                            var end_time = response.data[0]['dayInformation'][j]['end_time'];
	                            var end_time1 = end_time.split(':');
	                                end_time1 = end_time1[0]+':'+end_time1[1];

	                            var evening_start_time=response.data[0]['dayInformation'][j]['evening_start_time'];
	                            var evening_start_time1=evening_start_time.split(':');
	                                evening_start_time1=evening_start_time1[0]+':'+evening_start_time1[1];

	                            var evening_end_time = response.data[0]['dayInformation'][j]['evening_end_time'];
	                            var evening_end_time1 = evening_end_time.split(':');
	                                evening_end_time1 = evening_end_time1[0]+':'+evening_end_time1[1];    
										
								$(getInstanse).prop( "checked", true );
								$('.mndfrom'+getValue).removeAttr('disabled').val(start_time1);
								$('.mndto'+getValue).removeAttr('disabled').val(end_time1);
								$('.mndfrome'+getValue).removeAttr('disabled').val(evening_start_time1);
								$('.mndtoe'+getValue).removeAttr('disabled').val(evening_end_time1);
								$('#peakHourName').val(response.data[0]['dayInformation2'][0]['peak_hour_name'])
								getLastArray.push(j);
							}
						}
						getLastArray.push(j);

					});
                   $('#refresh_overlay').css("display","none");
				}
				else
				{
					alert("Data Not Found");
					$('#refresh_overlay').css("display","none");
				}

		    }
	    });
	},

    /*---------------------------------------------
      Function Name: deletePeakHourTimeDay()
      Input Parameter: rowId
      return:json data
   ---------------------------------------------*/
	deletePeakHourTimeDay:function(rowId){
	    $.ajax({
		    url: PeakHourMain._Serverpath,
		    type: 'POST',
		    dataType:'json',
		    data:"action=deletePeakHourTimeDay&rowId="+rowId,
		    success: function(response) {
			    PeakHourMain.getPeakHour();
		    }
	    });

	},

   /*---------------------------------------------
      Function Name: getPeakHour()
      Input Parameter: rowId
      return:json data
   ---------------------------------------------*/
	getPeakHour:function()
	{
		  $('#refresh_overlay').css("display","block");
		var getUserId=window.localStorage.getItem('companyInfo');
         getUserId=JSON.parse(getUserId);
		$.ajax({
			url: PeakHourMain._Serverpath,
			type: 'POST',
			dataType:'json',
			data:"action=getPeakHour&user_id="+getUserId[0].id,
			success: function(response) {
				var responseHTMl='';
	            if(response!=null){
					$.each(response.data,function(index,result){
						responseHTMl+='<tr><td>'+result.peak_hour_name+'</td><td>'+result.id+'</td><td><button class="btn btn-primary viewBtn" seq="'+result.id+'">View</button></td><td><button class="btn btn-primary editBtn" seq="'+result.id+'">Edit</button>&nbsp;&nbsp<button class="btn btn-primary deleteBtn" seq="'+result.id+'">Delete</button></td></tr>';
					});
				}else{
					responseHTMl+='<tr><td colspan="1">Data not Found.</td></tr>';
				}

				$('#peak_hour_list').html(responseHTMl);
				$('#refresh_overlay').css("display","none");	
				$('.deleteBtn').on("click",function(){
					$('#refresh_overlay').css("display","block");
					var getSeq=$(this).attr('seq');
					var getAnd=confirm("Are you sure for delete item");
					//$('#refresh_overlay').css("display","none");
					if(getAnd)
					{
						PeakHourMain.deletePeakHourTimeDay(getSeq);
					}
				});
				$('.viewBtn').on("click",function(){
					var getSeq=$(this).attr('seq');
					PeakHourMain.showPeakHourTimeDay(getSeq);
					$('#showBtnDiv').css({"display":"none"});
					$('#backBtnBack').css({"display":"block"});
				});
				$('.editBtn').on("click",function(){
					
					var getSeq=$(this).attr('seq');
					PeakHourMain.showPeakHourTimeDay(getSeq);
	             
					$('#showBtnDiv').css({"display":"block"}).find('.SaveBtn').val("Update").attr("seq",getSeq);
					$('#backBtnBack').css({"display":"block"});

				});

		    }
	    });

	},

    /*---------------------------------------------
      Function Name: setPeakHour()
      Input Parameter: getTotalJson
      return:json data
    ---------------------------------------------*/
	setPeakHour:function(getTotalJson)
	{
	
	    $.ajax({
			url: PeakHourMain._Serverpath,
			type: 'POST',
			dataType:'json',
			data:getTotalJson,
			success: function(response) {
				location.reload();
			}
	    });

	}

}



$('#peakHourAddForm').on("submit",function(eve){
	$('#refresh_overlay').css("display","block");
	eve.preventDefault();
    var btnValue=$('.SaveBtn').val();
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string")
    {
      getUserId=JSON.parse(getUserId);
    }
    var userId = getUserId[0].id;
    var getTime=[];
	if(btnValue!="Update")
	{
		var getArray=[];
		$('.mnd_check').each(function(){
			var getSeq=$(this).attr("value");
		    if($('.mndfrom'+getSeq).attr("disabled")==undefined)
			{
				var getDayName=$('.dayName'+getSeq).html().trim();
				var timeFrom=$('.mndfrom'+getSeq).val()
				var timeTo=$('.mndto'+getSeq).val()
				getArray.push({"dayName":getDayName,"timeFrom":timeFrom,"timeTo":timeTo});
				var timeEveningFrom=$('.mndfrome'+getSeq).val()
				var timeEveningTo=$('.mndtoe'+getSeq).val()
				getTime.push({"dayName":getDayName,"EveningTimeFrom":timeEveningFrom,"EveningTimeTo":timeEveningTo});
			}
		});
		var user_id  = userId;
		var getPeakHourName=$('#peakHourName').val();
		var getTotalJson={"peakHourName":getPeakHourName,
		"totaldayTime":getArray	,"totalEveningTime":getTime,"user_id":user_id,
		"action":"setPeakHour"
		}
		PeakHourMain.setPeakHour(getTotalJson);
		//$('#refresh_overlay').css("display","none");
	} 
	else
	{  
		$('#refresh_overlay').css("display","block");
		var btnValue=$('.SaveBtn').attr("seq");
		var getArray=[];
	    $('.mnd_check').each(function(){
			var getSeq=$(this).attr("value");
			if($('.mndfrom'+getSeq).attr("disabled")==undefined)
			{
				var getDayName=$('.dayName'+getSeq).html().trim();
				var timeFrom=$('.mndfrom'+getSeq).val()
				var timeTo=$('.mndto'+getSeq).val()
				getArray.push({"dayName":getDayName,"timeFrom":timeFrom,"timeTo":timeTo});
		
			    var timeEveningFrom=$('.mndfrome'+getSeq).val()
				var timeEveningTo=$('.mndtoe'+getSeq).val()
				getTime.push({"dayName":getDayName,"EveningTimeFrom":timeEveningFrom,"EveningTimeTo":timeEveningTo});

			}

		});
		var getPeakHourName=$('#peakHourName').val();
		var getTotalJson={"peakHourName":getPeakHourName,
		"totaldayTime":getArray	,
		"totalEveningTime":getTime,
		"action":"updatePeakHour",
		"rowId":btnValue
		}
		PeakHourMain.setPeakHour(getTotalJson);
		$('#refresh_overlay').css("display","none");
	}

  });
	PeakHourMain.getPeakHour();