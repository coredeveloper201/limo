/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Airport
  Author: Mylimoproject
---------------------------------------------*/

/*set the phpfile path for the webservice call*/
var _SERVICEPATH="phpfile/service.php";
var _SERVICEPATHServer="phpfile/client.php";
var _SERVICEPATHSma="phpfile/sma_client.php";
var _SERVICEPATHManFees="phpfile/cond_surcharge_client.php";
var ADDMILEAGE =0;

/*call the onload function on page onload*/
$(function(){	

	$('#back_button').on("click",function(){
		location.reload(true);
	});
    
    /*call the required default value on page load*/
	getSMA();
	getVehicleType();	
	
});


/*declare a class for the stop calculation page*/
var StopServiceClass={
	 /*web service php path for the client*/
	_SERVICEPATHSma:"phpfile/sma_client.php",
    
   /*---------------------------------------------
       Function Name: updateStopService()
       Input Parameter:user_id
       return:json data
    ---------------------------------------------*/
	updateStopService:function(jsonFullData){

		$.ajax({
		    url:StopServiceClass._SERVICEPATHSma,
		    type: 'POST',
		    data: jsonFullData,
		    success: function(response) {

			    alert('Successfully Update');
			    location.reload();
		    }
	    });
    
	},

	/*---------------------------------------------
        Function Name: updateStopService()
        Input Parameter:user_id
        return:json data
    ---------------------------------------------*/ 
    setStopService:function(jsonFullData){

		$.ajax({
			url:StopServiceClass._SERVICEPATHSma,
			type: 'POST',
			data: jsonFullData,
		    success: function(response) {
				alert('Successfully inserted');
				location.reload();
			}
		});
	},
 
    /*---------------------------------------------
        Function Name: updateStopService()
        Input Parameter:user_id
        return:json data
    ---------------------------------------------*/  
	deleteStopService:function(rowId)
	{
		$('#refresh_overlay').css("display","block");
		var jsonFullData={"rowId":rowId,"action":"deleteStopService"};

		$.ajax({
		    url:StopServiceClass._SERVICEPATHSma,
		    type: 'POST',
		    data: jsonFullData,
		    success: function(response) {
			    StopServiceClass.getStopService();

		    }
	    });
	},
    /*---------------------------------------------
        Function Name: viewStopService()
        Input Parameter:jsonFullData
        return:json data
    ---------------------------------------------*/  
	viewStopService:function(rowId){

	    $('#refresh_overlay').css("display","block");
	    var jsonFullData={"rowId":rowId,"action":"viewStopService"};

		$.ajax({
		    url:StopServiceClass._SERVICEPATHSma,
		    type: 'POST',
		    data: jsonFullData,
		    success: function(response) {
			    var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				if(responseObj.code==1003)
				{
					var responseHTML='';
					$.each(responseObj.data,function(index,resultFull){
						responseHTML+='<tr><td>'+resultFull.stops+'</td><td>'+resultFull.stops_wait+'</td><td>'+resultFull.fixed_amt+'</td><td>'+resultFull.hourly_rate+'%</td><td>'+resultFull.disclaimer_discription+'</td></tr>';
				    });
				    $('#showStop_wait_service').html(responseHTML);
			    }
			    $('#refresh_overlay').css("display","none");
			    $('#postal_code_modal').css("display","block");
		    }
	    });
	},

    /*---------------------------------------------
       Function Name: viewStopService()
       Input Parameter:jsonFullData
       return:json data
    ---------------------------------------------*/   
	viewStopServiceSpecific:function(rowID)
	{
		$('#refresh_overlay').css("display","block");
		var jsonFullData={"rowID":rowID,"action":"viewStopServiceSpecific"};

		$.ajax({
		    url:StopServiceClass._SERVICEPATHSma,
		    type: 'POST',
		    data: jsonFullData,
		    success: function(response) {
			    var responseOBJ='';
			    if(typeof(response)=="string")
			    {
				    responseOBJ=JSON.parse(response);
			    }
				var createDiv=0;
				var responseHTMLOption1=$('.getStopServiceValue0').html();
					$('#addNewSetting').html("");
					$('#addNewSetting').html('<div class="row getStopServiceValue getStopServiceValue0 " seq="0"  >'+responseHTMLOption1+'</div>');
			    $.each(responseOBJ.data[0].stop_cal_db_data,function(index,resultResponsetwo){
					if(createDiv==0)
					{
						if(resultResponsetwo.hourly_rate=="0")
							{
								$('#hourly_amt').val('');
							}
							else
							{
								$('#hourly_amt').val(resultResponsetwo.hourly_rate);
							}
							$('#apply_stop').val(resultResponsetwo.stops);
							$('#peakHourTime').val(resultResponsetwo.stops_wait);
							$('#fixed_amt').val(resultResponsetwo.fixed_amt);
							
							$('#disclaimer_discription').val(resultResponsetwo.disclaimer_discription);
							if(resultResponsetwo.status==2)
							$('#disclaimer_checkout').attr("checked","true");
						}
						else
						{
						   if(resultResponsetwo.hourly_rate=="0")
							{
								resultResponsetwo.hourly_rate='';
							}
							var checkedValue='';
							var responseHTMLOption='';
							if(resultResponsetwo.status==2)
								checkedValue="checked";
							        responseHTMLOption+='<div class=" row getStopServiceValue delete_stop_setting_div_'+createDiv+'" seq='+createDiv+'> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;" >Next:  </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;" >  <input type="text" class="form-control" id="apply_stop" value='+resultResponsetwo.stops+' placeholder="Stop">  </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left"> <select id="peakHourTime" class="form-control peakHourTime" required style="height:34px"> ';
							for(var i=1; i<=60; i++)
							{
								if(resultResponsetwo.stops_wait==i)
								{
								  	responseHTMLOption+='<option value='+i+' selected>'+i+'</option>';
								}
								else
								{
								  	responseHTMLOption+='<option value='+i+'>'+i+'</option>';		
								}
						    }

							responseHTMLOption+='</select>Minutes </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;" > <input type="text" class="form-control fixedAmountOnChange  fixedAmount_'+createDiv+'" seq="'+createDiv+'" id="fixed_amt" value="'+resultResponsetwo.fixed_amt+'" placeholder=""> </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:center;" > <input type="text" class="form-control hourlyOnChange hourly_'+createDiv+'" seq="'+createDiv+'" id="hourly_amt" value="'+resultResponsetwo.hourly_rate+'"  placeholder="Hourly Amount"> </div> <div class="col-sm-2" style="text-align:center;" > <input type="text" class="form-control" id="disclaimer_discription" value="'+resultResponsetwo.disclaimer_discription+'" placeholder="Confirmation disclaimer content"> </div> <div class="col-xs-1 col-sm-1 col-md-1"> <input type="checkbox" style="height: 19px;padding-left: 3.5%"  class="form-control" id="disclaimer_checkout" '+checkedValue+' placeholder="Confirmation disclaimer content"> </div> <div class="col-xs-2 col-sm-2 col-md-2" style="text-align:center;" > <input type="button" name="add-stop-setting " class="delete_stop_setting" seq='+createDiv+' value="Delete"></div> </div> ';
						    $('#addNewSetting').append(responseHTMLOption);
						    validationOnAmount();
						}

					createDiv++;
     		    });

				$('#add_stop_setting').on("click",function(){
					createDivRow=0;
					$('.getStopServiceValue').each(function(){
						createDivRow++;
					});
					var responseHTMLOption2='';
					for(var i=1; i<=60; i++)
				    {
					   responseHTMLOption2+='<option value='+i+'>'+i+'</option>';	

				    }
					$('#addNewSetting').append(' <div class=" row getStopServiceValue delete_stop_setting_div_'+createDivRow+'" seq='+createDivRow+'> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:right;" >Next:  </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;" >  <input type="text" class="form-control" id="apply_stop" placeholder="Stop">  </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left"> <select id="peakHourTime" class="form-control peakHourTime" required style="height:34px">'+responseHTMLOption2+' </select>Minutes </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;" > <input type="text" class="form-control fixedAmountOnChange  fixedAmount_'+createDivRow+'" seq="'+createDivRow+'" id="fixed_amt" placeholder=""> </div> <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:center;" > <input type="text" class="form-control hourlyOnChange hourly_'+createDivRow+'" seq="'+createDivRow+'"  id="hourly_amt" placeholder="% of Hrly Rate"> </div> <div class="col-xs-2 col-sm-2 col-md-2" style="text-align:center;" > <input type="text" class="form-control" id="disclaimer_discription" placeholder="Confirmation disclaimer content"> </div> <div class="col-xs-1 col-sm-1 col-md-1"> <input type="checkbox" style="height: 19px; padding-left: 3.5%"  class="form-control" id="disclaimer_checkout" placeholder="Confirmation disclaimer content"> </div> <div class="col-xs-2 col-sm-2 col-md-2" style="text-align:center;" > <input type="button" name="add-stop-setting " class="delete_stop_setting" seq='+createDivRow+' value="Delete"></div> </div> ');
					 validationOnAmount();
				});
				$('.delete_stop_setting').on("click",function(){
				    var getSeq=$(this).attr("seq");
				    $('.delete_stop_setting_div_'+getSeq).remove();

				});
			    $('#stop_service_name').val(responseOBJ.data[0].master_stop_cal);
			        var service_typeArray=[];
					var vehicle_codeArray=[];
					var apply_smaArray=[];
			    $.each(responseOBJ.data[0].stop_cal_services_data,function(index,resultResponse){
			
					service_typeArray.push(resultResponse.service_type);
					vehicle_codeArray.push(resultResponse.vehicle_code);
					apply_smaArray.push(resultResponse.sma_name);	
			    });

			    $('#apply_sma').val(apply_smaArray);
			    $("#apply_sma").multiselect('destroy');
			    $('#apply_sma').multiselect({
			        maxHeight: 400,
			        buttonWidth: '155px',
			        includeSelectAllOption: true
			    });
			    $('#vehicle_code').val(vehicle_codeArray);
			    $("#vehicle_code").multiselect('destroy');
			    $('#vehicle_code').multiselect({
			        maxHeight: 400,
			        buttonWidth: '155px',
			        includeSelectAllOption: true
			    });
			    $('.service_type').val(service_typeArray);
			    $(".service_type").multiselect('destroy');
			    $('.service_type').multiselect({
			        maxHeight: 400,
			        buttonWidth: '155px',
			        includeSelectAllOption: true
			    });
			    $('#refresh_overlay').css("display","none");

		    }
	   });

    },
		
	/*---------------------------------------------
       Function Name: getServiceTypes()
       Input Parameter:user_id
       return:json data
    ---------------------------------------------*/	
    getServiceTypes:function()
    {
	    var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;

		if(typeof(userInfo)=="string")
	    {
		    userInfoObj=JSON.parse(userInfo);
	    } 
        var serviceTypeData = [];
        $.ajax({
            url : "phpfile/service_type.php",
            type : 'post',
            data : 'action=GetServiceTypes&user_id='+userInfoObj[0].id,
            dataType : 'json',
            success : function(data){
         
                if(data.ResponseText == 'OK'){
                    var ResponseHtml='';
                    $.each(data.ServiceTypes.ServiceType, function( index, result){

                        ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
                    
                    });
             
                    $('#apply_service').html(ResponseHtml);
                }

                $("#apply_service").multiselect('destroy');
            	$('#apply_service').multiselect({
                    maxHeight: 400,
                    buttonWidth: '178px',
                    includeSelectAllOption: true
                });
            }
        });
    },
		
   /*---------------------------------------------
      Function Name: getServiceTypes()
      Input Parameter:user_id
      return:json data
    ---------------------------------------------*/		
	getStopService:function()
	{
	    $('#refresh_overlay').css("display","block");
	    var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
	    {
	        getUserId=JSON.parse(getuserId);
	    }
	    var jsonFullData={"action":"getStopService","user_id":getUserId[0].id};
		$.ajax({
		    url:StopServiceClass._SERVICEPATHSma,
		    type: 'POST',
		    data: jsonFullData,
		    success: function(response) {
			    var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);

				}
				
				var responseObjHTML='';
				if(responseObj.code==1003)
				{
				   $.each(responseObj.data,function(index,response){

						responseObjHTML+='<tr><td>'+response.master_stop_cal+'</td><td><select ">';
						var responseObjHTMLservice='';
						var responseObjHTMLvehicle='';
						var distinctServiceTypeArray=[];
						var distinctVehicleTypeArray=[];
						$.each(response.stop_cal_services_data,function(index,responseinner){
							if( $.inArray(responseinner.service_type, distinctServiceTypeArray) == -1 ){
							    
							    distinctServiceTypeArray.push(responseinner.service_type);
							    responseObjHTMLservice+='<option>'+responseinner.service_type+'</option>';
							}

						    if( $.inArray(responseinner.vehicle_code, distinctVehicleTypeArray) == -1 ){
								distinctVehicleTypeArray.push(responseinner.vehicle_code);
							    responseObjHTMLvehicle+='<option>'+responseinner.vehicle_code+'</option>';
						    }
						});
						    responseObjHTML+=responseObjHTMLservice+'</select></td><td><select  ">'+responseObjHTMLvehicle+'</select></td><td><button class="btn btn-primary viewStopService" seq='+response.master_stop_cal_id+'>view</button></td><td><button class="btn btn-primary editstop" seq='+response.master_stop_cal_id+' >Edit</button>&nbsp;&nbsp;<button class="btn btn-primary deleteStop" seq='+response.master_stop_cal_id+'>Delete</button></td></tr>';
					});

			    }else{

			     responseObjHTML='<tr><td colspan="1">Data not Found.</td></tr>';  
			    }
			    $('#refresh_overlay').css("display","none");
				$('#rate_matrix_list').html(responseObjHTML);
				$('.editstop').on("click",function(){

					var getseq=$(this).attr("seq");
					$('#save_rate').html("Update");
					$('#save_rate').attr("seq",getseq);
					$('#back_button').css("display","block");
					StopServiceClass.viewStopServiceSpecific(getseq);
				});
				$('.deleteStop').on("click",function(){
					var getseq=$(this).attr("seq");
					var getAns=confirm("Are you Sure !");
					if(getAns)
					{
						StopServiceClass.deleteStopService(getseq);	
				    }
				
				});
				$('.viewStopService').on("click",function(){
				    var getSeq=$(this).attr("seq");
						StopServiceClass.viewStopService(getSeq);
				});
			    $('#refresh_overlay').css("display","none");

		    }
	    });

    }

}
      
    /*---------------------------------------------
      Function Name: getServiceTypes()
      Input Parameter:user_id
      return:json data
    ---------------------------------------------*/		     
    function getSMA()
	{
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
		$.ajax({
		    url: _SERVICEPATHSma,
		    type: 'POST',
			data: "action=getSMA&user_id="+userInfoObj[0].id,
			success: function(response) {
			    var responseObj=response;
			    var responseHTML='';
			    if(typeof(response)=="string")
			    {
				    responseObj=JSON.parse(response);
			    }	
			    var responseHTML='';
           	    for(var i=0; i<responseObj.data.length; i++)
			    {	
		            if(responseObj.data[i].id!=null){
					    responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
		            }
			    }
          
				$('#apply_sma').html(responseHTML);
				setTimeout(function(){				
				$("#apply_sma").multiselect('destroy');
					$('#apply_sma').multiselect({
					    maxHeight: 400,
					    buttonWidth: '155px',
					    includeSelectAllOption: true
					});

			    },400);
			},
			    error:function(){
			    alert("Some Error");
			}
			
		});
	}
 
    /*cal the setstopservice on load of page*/
	StopServiceClass.getStopService();
    
    /*---------------------------------------------
      Function Name: getVehicleType()
      Input Parameter:user_id
      return:json data
    ---------------------------------------------*/	
	function getVehicleType()
	{
		var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	    if(typeof(getLocalStoragevalue)=="string")
	    {
	     	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);

        }

        var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}
	    $.ajax({
		    url: _SERVICEPATH,
		    type: 'POST',
		    data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
		    success: function(response) {
			    var responseHTML='';
			    var responseObj=response;
			    if(typeof(response)=="string")
			    {
				    responseObj=JSON.parse(response);					
			    }

			    for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
			    {
			      responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
			    }
			    $('#vehicle_code').html(responseHTML);
			    setTimeout(function(){
				    $("#vehicle_code").multiselect('destroy');
            	    $('#vehicle_code').multiselect({
                        maxHeight: 400,
                        buttonWidth: '155px',
                        includeSelectAllOption: true
                    });
			
				},400);

		    }

	    });
	}

    /*---------------------------------------------
      Function Name: click on stopcalculation form
      Input Parameter:all the form field value
      return:json data
    ---------------------------------------------*/	
	$('#stopCalSolutionForm').on("submit",function(eve){
		eve.preventDefault();
        $('#refresh_overlay').css("display","block");
		if($('#save_rate').html()!='Update')
		{
			var occasion= $('.service_type option:selected');
		    var vehicle_code= $('#vehicle_code option:selected');
            var apply_sma= $('#apply_sma option:selected');
            var apply_smaArray = [];
            $(apply_sma).each(function(index, apply_smaResult){
                apply_smaArray.push($(this).val());
            });
            var occasionObject = [];
	        $(occasion).each(function(index, occasion){
	            occasionObject.push($(this).val());
	        });

            var vehicle_code_Array = [];
            $(vehicle_code).each(function(index, vehicle_codeResult){
                vehicle_code_Array.push($(this).val());
            });
       
            var fullArraystopTimeservice=[];
	        $('.getStopServiceValue').each(function(index,result){

	        	var getSeq=$(this).attr('seq');
	        	var getStops=$(this).find('#apply_stop').val();
	        	var peakHourTime=$(this).find('#peakHourTime').val();
	        	var fixed_amt=$(this).find('#fixed_amt').val();
	        	var hourly_amt=$(this).find('#hourly_amt').val();
	        	var disclaimer_discription=$(this).find('#disclaimer_discription').val();
	        	var disclaimer_checkout=$(this).find('#disclaimer_checkout').is(":checked");
	        	var finalCheckBoxValue=1;
	        	if(disclaimer_checkout==true)
	        	{
	        		finalCheckBoxValue=2;
	        	}
	        	fullArraystopTimeservice.push({"stops":getStops,"per_stop_wait":peakHourTime,"fixed_amt":fixed_amt,
	        		"hourly_amt":hourly_amt,"disclaimer_discription":disclaimer_discription,"disclaimer_checkout":finalCheckBoxValue});
	        });

            var getUserID=window.localStorage.getItem("companyInfo");
			if(typeof(getUserID)=="string")
			{
			   	getUserID=JSON.parse(getUserID);
			}
            var getStopServiceName=$('#stop_service_name').val();
            var getFullJsonData={"service_type":occasionObject,
         					  "vehicle_code":vehicle_code_Array,
         					  "apply_sma":apply_smaArray,
         					  "user_id":getUserID[0].id,
         					  "getStopServiceName":getStopServiceName,
         					  "fullArraystopTimeservice":fullArraystopTimeservice,
         					  "action":"setStopService"
            };
            StopServiceClass.setStopService(getFullJsonData);
		}
		else
		{

			var getSeqValue=$('#save_rate').attr("seq");
			var occasion= $('.service_type option:selected');
		    var vehicle_code= $('#vehicle_code option:selected');
		    var apply_sma= $('#apply_sma option:selected');
            var apply_smaArray = [];
            $(apply_sma).each(function(index, apply_smaResult){
                apply_smaArray.push($(this).val());
            });
  
            var occasionObject = [];
            $(occasion).each(function(index, occasion){
                occasionObject.push($(this).val());
            });

            var vehicle_code_Array = [];
            $(vehicle_code).each(function(index, vehicle_codeResult){
                 vehicle_code_Array.push($(this).val());

            });
       
            var fullArraystopTimeservice=[];
            $('.getStopServiceValue').each(function(index,result){
        	    
        	    var getSeq=$(this).attr('seq');
        	    var getStops=$(this).find('#apply_stop').val();
        	    var peakHourTime=$(this).find('#peakHourTime').val();
        	    var fixed_amt=$(this).find('#fixed_amt').val();
        	    var hourly_amt=$(this).find('#hourly_amt').val();
        	    var disclaimer_discription=$(this).find('#disclaimer_discription').val();
        	    var disclaimer_checkout=$(this).find('#disclaimer_checkout').is(":checked");
        	    var finalCheckBoxValue=1;
        	    if(disclaimer_checkout==true)
        	    {
        		   finalCheckBoxValue=2;
        	    }
        	
        	    fullArraystopTimeservice.push({"stops":getStops,"per_stop_wait":peakHourTime,"fixed_amt":fixed_amt,
        		"hourly_amt":hourly_amt,"disclaimer_discription":disclaimer_discription,"disclaimer_checkout":finalCheckBoxValue});
            });

            var getStopServiceName=$('#stop_service_name').val();
            var getFullJsonData={"service_type":occasionObject,
         					  "vehicle_code":vehicle_code_Array,
         					  "apply_sma":apply_smaArray,
         					  "getStopServiceName":getStopServiceName,
         					  "fullArraystopTimeservice":fullArraystopTimeservice,
         					  "action":"updateStopService",
         					  "getSeqValue":getSeqValue
            };

            StopServiceClass.updateStopService(getFullJsonData);

		}

	});

	



				