
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: user profile
  Author: Mylimoproject
---------------------------------------------*/
/*php file link for webservice call*/
var _SERVICEPATH="phpfile/client.php";

getUserProfileInfo();


/*---------------------------------------------
   Function Name: getUserProfileInfo
   Input Parameter: rowId,categoryId
   return:json data
 ---------------------------------------------*/

	function getUserProfileInfo()
	{
       $('#refresh_overlay').css("display","block");
	    var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");

		if(typeof(getLocalStoragevalueUserInformation)=="string")
		{
		   	getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);

		}else{

		   window.location.href="login.html";	
	    }

		$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
		$('#userId').val(getLocalStoragevalueUserInformation[0].full_name);
		$('#mbNumber').val(getLocalStoragevalueUserInformation[0].mobile_number);
		$('#companyName').val(getLocalStoragevalueUserInformation[0].company_name);
	   	$('#password').val(getLocalStoragevalueUserInformation[0].password);
		$('#emailId').val(getLocalStoragevalueUserInformation[0].email);
	    $('#userAuthKey').html(getLocalStoragevalueUserInformation[0].user_auth_key);
	    var set_limo=getLocalStoragevalueUserInformation[0].limo_setup;
	    var isalaw_check=getLocalStoragevalueUserInformation[0].isLAWCheck;
	
	    if(set_limo=='off'){
	    	var setting_tog='on_off';
	    	var setting_sel=set_limo;

             $('#'+setting_tog).prop('value', setting_sel);
              $('a[data-toggle="'+setting_tog+'"]').not('[data-title="'+setting_sel+'"]').removeClass('active').addClass('notActive');
              $('a[data-toggle="'+setting_tog+'"][data-title="'+setting_sel+'"]').removeClass('notActive').addClass('active');
             localStorage.setItem('limo_mode',set_limo);
	    }else{
 
           localStorage.setItem('limo_mode',set_limo);
	    }
		$('#updateUserProfile').on("click",function(){

			$('#refresh_overlay').css("display","block");	
			var UserName=$('#userId').val();
			var mobileNumber=$('#mbNumber').val();
			var companyName=$('#companyName').val();
			var password=$('#password').val();
			var limoanywhere_setup=$('#on_off').val();
			var isalow_setup=isalaw_check;
			if(limoanywhere_setup=='off'){

                 isalow_setup=1;
			}else{

				isalow_setup=0;
			}
          
			if(UserName!='' && mobileNumber!='' && companyName!='' && password!='' && limoanywhere_setup!='')
			{
				updateUserProfile(UserName,mobileNumber,companyName,password,limoanywhere_setup,isalow_setup);

			}
		    else{
	          $('#refresh_overlay').css("display","none");	
					alert("All fields are required");

			}
		});

	}
/*---------------------------------------------
   Function Name: getUserProfileInfo
   Input Parameter: rowId,categoryId
   return:json data
 ---------------------------------------------*/
	function updateUserProfile(UserName,mobileNumber,companyName,password,limoanywhere_setup,isalow_setup)
	{
		$('#refresh_overlay').css("display","block");	
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalue)=="string")
		{
		   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}
        var userId=getLocalStoragevalue[0].id;
		$.ajax({
			url: _SERVICEPATH,
			type: 'POST',
			data: "action=updateUserProfile&UserName="+UserName+"&mobileNumber="+mobileNumber+"&companyName="+companyName+"&password="+password+"&userId="+userId+"&limo_setup="+limoanywhere_setup+"&isalow_setup="+isalow_setup,
			success: function(response) {
				$('#refresh_overlay').css("display","none");
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);					
				}
				console.log(responseObj);
			    window.localStorage.setItem("companyInfo",JSON.stringify(responseObj.data));
			    $('#refresh_overlay').css("display","none");
				//location.reload();

			}

		});

    }