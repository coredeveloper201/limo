var zoneToZoneTollMatix = {
    _Serverpath: "phpfile/rate_point_client.php",
    _SERVICEPATHCAR: "phpfile/service.php",
    pageLoadFunction: function() {
        var zoneCity = window.localStorage.getItem('zoneCity');
        $('#tollZone-A-smaSetup').on('change', function() {



            var getSmaId = $('#tollZone-A-smaSetup').val();
            var getUserId = window.localStorage.getItem('companyInfo');
            


            if (typeof(getUserId) == "string") {
                getUserId = JSON.parse(getUserId);
            }
            $.ajax({
                url: getPointToPointClass._Serverpath,
                type: 'POST',
                data: "action=getzoneTozoneCitySma&sma_id=" + getSmaId + "&user_id=" + getUserId[0].id,
                success: function(response) {
                    var responseOBJ = '';
                    if (typeof(response) == "string") {
                        responseOBJ = JSON.parse(response);
                    }
                    var getHtml = '';
                    var getPostalHtml = '';
                    var availableTags = [];
                    if (responseOBJ.code == 1007) {
                        $.each(responseOBJ.data, function(index, result) {

                            availableTags.push(result.city_name);
                            getHtml += '<option value="' + result.city_id + '">' + result.city_name + '</option>';
                            getPostalHtml += '<option value="' + result.postal_code + '">' + result.postal_code + '</option>';
                        });

                       


                             $('#tollZone-A-citySetup').html(getHtml);
                        $("#tollZone-A-citySetup").multiselect('destroy');
                        $('#tollZone-A-citySetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });
                       
                       
                        // $('#tollZone-A-zipcodeSetup').html(getPostalHtml);
                        // $("#tollZone-A-zipcodeSetup").multiselect('destroy');
                        // $('#tollZone-A-zipcodeSetup').multiselect({
                        //     maxHeight: 200,
                        //     buttonWidth: '178px',
                        //     includeSelectAllOption: true
                        // });



                     setTimeout(function(){

                           if (typeof(zoneCity) == "string") {

                            var zoneCityLocal = JSON.parse(zoneCity);
                            // window.localStorage.removeItem('zoneCity');
                            var getArrayJSOn = [];
                            $.each(zoneCityLocal, function(index, result) {

                                if (result.type == "from") {
                                    getArrayJSOn.push(result.city_id);

                                }
                            });
                         
                            $("#tollZone-A-citySetup").val(getArrayJSOn);
                            $("#tollZone-A-citySetup").multiselect('refresh');

                            $('#toll_zone_a_city_check').click()
                        }

                     },1500)




                    }
                }
            });
        });
        $('#tollZone-B-smaSetup').on('change', function() {
            var getSmaId = $('#tollZone-B-smaSetup').val();
            var getUserId = window.localStorage.getItem('companyInfo');
            if (typeof(getUserId) == "string") {
                getUserId = JSON.parse(getUserId);
            }
            $.ajax({
                url: getPointToPointClass._Serverpath,
                type: 'POST',
                data: "action=getzoneTozoneCitySma&sma_id=" + getSmaId + "&user_id=" + getUserId[0].id,
                success: function(response) {
                    var responseOBJ = '';
                    if (typeof(response) == "string") {
                        responseOBJ = JSON.parse(response);
                    }
                    var getHtml = '';
                    var getPostalHtml = '';
                    var availableTags = [];
                    if (responseOBJ.code == 1007) {
                        $.each(responseOBJ.data, function(index, result) {
                            availableTags.push(result.city_name);
                            getHtml += '<option value="' + result.city_id + '">' + result.city_name + '</option>';
                            getPostalHtml += '<option value="' + result.postal_code + '">' + result.postal_code + '</option>';
                        });
                        $('#tollZone-B-citySetup').html(getHtml);
                        $("#tollZone-B-citySetup").multiselect('destroy');
                        $('#tollZone-B-citySetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });

                        // $('#tollZone-B-zipcodeSetup').html(getPostalHtml);
                        // $("#tollZone-B-zipcodeSetup").multiselect('destroy');
                        // $('#tollZone-B-zipcodeSetup').multiselect({
                        //     maxHeight: 200,
                        //     buttonWidth: '178px',
                        //     includeSelectAllOption: true
                        // });


                           setTimeout(function(){

                           if (typeof(zoneCity) == "string") {

                            var zoneCityLocal = JSON.parse(zoneCity);
                            window.localStorage.removeItem('zoneCity');
                            var getArrayJSOn = [];
                            $.each(zoneCityLocal, function(index, result) {

                                if (result.type == "to") {
                                    getArrayJSOn.push(result.city_id);

                                }
                            });
                         
                            $("#tollZone-B-citySetup").val(getArrayJSOn);
                            $("#tollZone-B-citySetup").multiselect('refresh');
                            $('#toll_zone_a_city_check').click()


                        }

                     },1500);




                    }
                }
            });
        });


        $('#toll_zone_b_city_check').on("click",function(){



            var tollZoneCityA = $('#tollZone-B-citySetup option:selected');
            var countryId = $('#tollZone-B-smaSetup').val();
            if(service_type.length!=0){
                var tollZoneCityArray = [];
                $(tollZoneCityA).each(function(index, selectedState){
                    tollZoneCityArray.push($(this).text());
                });


                var getUserId=window.localStorage.getItem('companyInfo');
                    getUserId=JSON.parse(getUserId);
                var  getJson={
                        "tollZone_A_smaSetup":tollZoneCityArray,
                        "user_id":getUserId[0].id,
                        "countryId":countryId,
                        "action":"getZoneZipCodes"
                    };
                    $.ajax({
            url: getPointToPointClass._Serverpath,
            type: 'POST',
            data:getJson,
            dataType:'json',
            success: function(response) {
                var getHtml='';
                 $.each(response.data, function(index, result) {
                            // availableTags.push(result.city_name);
                            
                            getHtml += '<option value="'+result+'">' + result + '</option>';
                            // getPostalHtml += '<option value="' + result.postal_code + '">' + result.postal_code + '</option>';
                        });
                        $('#tollZone-B-zipcodeSetup').html(getHtml);

                        $("#tollZone-B-zipcodeSetup").multiselect('destroy');
                        $('#tollZone-B-zipcodeSetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });


                         setTimeout(function(){
                            var getZipLocalValue=localStorage.getItem('zoneZip');

                                if(typeof getZipLocalValue == "string")
                                {

                                        getZipLocalValue=JSON.parse(getZipLocalValue);
                                        var getALlZipCod=[];

                                        $.each(getZipLocalValue,function(zipindex,zipResult){
                                            if(zipResult.type=="to")
                                            {
                                                console.log("zipResult...",zipResult);

                                                    getALlZipCod.push(zipResult.postal_code);
                                            }


                                        });;

                                        $("#tollZone-B-zipcodeSetup").val(getALlZipCod);
                                        $("#tollZone-B-zipcodeSetup").multiselect('refresh');
                                }


                        },1000)




            }   
        });


                console.log("tollZoneCityArray...",tollZoneCityArray);
            }
       



            

        })
        
        $('#toll_zone_a_city_check').on('click',function(){

// tollZone-A-citySetup

var tollZoneCityA = $('#tollZone-A-citySetup option:selected');
            if(service_type.length!=0){
                var tollZoneCityArray = [];
                $(tollZoneCityA).each(function(index, selectedState){
                    tollZoneCityArray.push($(this).text());
                });
                var countryId=$("#tollZone-A-smaSetup").val();

                var getUserId=window.localStorage.getItem('companyInfo');
                    getUserId=JSON.parse(getUserId);
                var  getJson={
                        "tollZone_A_smaSetup":tollZoneCityArray,
                        "user_id":getUserId[0].id,
                        "countryId":countryId,
                        "action":"getZoneZipCodes"
                    };





                    $.ajax({
            url: getPointToPointClass._Serverpath,
            type: 'POST',
            data:getJson,
            dataType:'json',
            success: function(response) {
                var getHtml='';
                 $.each(response.data, function(index, result) {
                            // availableTags.push(result.city_name);
                            console.log("result...",result);
                            getHtml += '<option value="'+result+'">' + result + '</option>';
                            // getPostalHtml += '<option value="' + result.postal_code + '">' + result.postal_code + '</option>';
                        });
                        $('#tollZone-A-zipcodeSetup').html(getHtml);

                        $("#tollZone-A-zipcodeSetup").multiselect('destroy');
                        $('#tollZone-A-zipcodeSetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });

                        setTimeout(function(){
                            var getZipLocalValue=localStorage.getItem('zoneZip');

                                if(typeof getZipLocalValue == "string")
                                {

                                        getZipLocalValue=JSON.parse(getZipLocalValue);
                                        var getALlZipCod=[];

                                        $.each(getZipLocalValue,function(zipindex,zipResult){
                                            if(zipResult.type=="from")
                                            {

                                                    getALlZipCod.push(zipResult.postal_code);
                                            }


                                        });;

                                        $("#tollZone-A-zipcodeSetup").val(getALlZipCod);
                                        $("#tollZone-A-zipcodeSetup").multiselect('refresh');
                                }


                        },1000)




            }   
        });


                console.log("tollZoneCityArray...",tollZoneCityArray);
            }
        })


    },
    getZoneACity:function(){

    }
};
zoneToZoneTollMatix.pageLoadFunction();