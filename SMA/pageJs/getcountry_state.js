/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: get country state
    Author: Mylimoproject
---------------------------------------------*/
/*create a class name for the get country*/
	var _SERVICEPATH="phpfile/airport_db_client.php";
	GetCountries();

	/*---------------------------------------------
       Function Name: GetCountries()
       Input Parameter: 
       return:json data
    ---------------------------------------------*/
	function GetCountries(){
		var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");		
		if(typeof(getLocalStoragevalueUserInformation)=="string"){
	   		getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);	
		}
		$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalue)=="string"){
	   		getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}
		
		$.ajax({
			url: _SERVICEPATH,
			type: 'POST',
			data: "action=getSmaCountry&user_id="+getLocalStoragevalue[0].id,
			success: function(response) {
				var ResponseHtml='<option value="">Select</option>';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}
				
	            $.each(responseObj.data,function(index,dataUse){                  
					if(dataUse.id!=null){
						ResponseHtml+="<option seq='"+dataUse.id+"' value='"+dataUse.country_name+"'>"+dataUse.country_name+"</option>";
					}
				});

				$('#country_name').html(ResponseHtml);
				$('.sma_country_list').on('change',function(){
		            $('#state_name').val('');
		            var country_id = $('option:selected', this).attr('seq');
		            localStorage.setItem('country_id',country_id);
		            getSmaState(country_id)    
				});								
			}	 
		});
	}


	/*---------------------------------------------
       Function Name:getSmaState()
       Input Parameter: country_id
       return:json data
    ---------------------------------------------*/
	function getSmaState(country_id){
		var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");		
		if(typeof(getLocalStoragevalueUserInformation)=="string"){
	   		getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);	
		}

		$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalue)=="string"){
	   		getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}
		
		$.ajax({
			url: _SERVICEPATH,
			type: 'POST',
			data: "action=getSmaState&country_id="+country_id+"&user_id="+getLocalStoragevalue[0].id,
			success: function(response){		
				var ResponseHtml='<option value="">Select</option>';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}
				
                console.log(responseObj);
				$.each(responseObj.data,function(index,dataUse){                     
					if(dataUse.id!=null){
						ResponseHtml+="<option seq='"+dataUse.id+"' value='"+dataUse.state_name+"'>"+dataUse.state_name+"</option>";
					}
				});

				$('#state_name').html(ResponseHtml);
				$('.sma_state_list').on('change',function(){
                    $('#city_name').val('');
                    var state_id = $('option:selected', this).attr('seq');
                    var country_id=localStorage.getItem('country_id');
                    getSmaCity(country_id,state_id);
				});
				
			}
	 	});
	}


	/*---------------------------------------------
       Function Name:getSmaCity()
       Input Parameter: country_id, state_id
       return:json data
    ---------------------------------------------*/
	function getSmaCity(country_id,state_id){
		var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalueUserInformation)=="string"){
		   	getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
		}

		$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalue)=="string"){
	   		getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}
	
		$.ajax({
			url: _SERVICEPATH,
			type: 'POST',
			data: "action=getSmaCity&country_id="+country_id+"&state_id="+state_id+"&user_id="+getLocalStoragevalue[0].id,
			success: function(response){
				var ResponseHtml='<option value="">Select</option>';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}
				
                console.log(responseObj);
				$.each(responseObj.data,function(index,dataUse){                      
					if(dataUse.id!=null){
						ResponseHtml+="<option  value='"+dataUse.city_name+"'>"+dataUse.city_name+"</option>";
					}
				});

				$('#city_name').html(ResponseHtml);
			}
	 	});
	}

