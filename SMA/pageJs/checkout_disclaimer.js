/*Set the disclaimer*/
var _SERVICEPATH = "phpfile/client.php";
/*click the update disclaimer button*/
$('#updateDisclaimerSetup').on('click', function() {

    $('#refresh_overlay').css("display", "block");
    var getLocalStoragevalue = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalue) == "string") {
        getLocalStoragevalue = JSON.parse(getLocalStoragevalue);
    }
    var userId = getLocalStoragevalue[0].id;



    var editor_contents = CKEDITOR.instances["editor"].getData();
    // var get_credit_card_disclaimer = editor_contents;
    var get_credit_card_disclaimer = $('#CCardDisclamer').val();
    var get_reservation_agreement = editor_contents;
    var get_cash_disclaimer = $('#cashPaymentDisclaimer').val();
    if (get_credit_card_disclaimer == '') {
        alert('Credit card disclaimer should not be blank.');
        $('.CCardDisclamer').focus();
        return false;
    }


    if (get_reservation_agreement == '') {

        alert('Reservatio Agreement should not be blank.');
        $('#editor').focus();
        return false;

    }

    var get_updated_id = $(this).attr('updated_id');
    if (get_updated_id != '') {

        $.ajax({
            url: _SERVICEPATH,
            type: 'POST',
            data: "action=updateDisclaimer&ccDiclaimer=" + get_credit_card_disclaimer + "&reservAgreement=" + get_reservation_agreement + "&get_cash_disclaimer=" + get_cash_disclaimer + "&updated_id=" + get_updated_id,
            success: function(response) {

                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }
                console.log(responseObj);
                if (responseObj.code == 1001) {

                    alert('Disclaimers updated successfully.');
                    getDisclaimer();
                    $('#refresh_overlay').css("display", "none");
                } else {

                    alert('Disclaimers not updated.');
                    $('#refresh_overlay').css("display", "none");
                }
                $('#refresh_overlay').css("display", "none");
                //location.reload();

            }

        });

    } else {
        $.ajax({
            url: _SERVICEPATH,
            type: 'POST',
            data: "action=setDisclaimer&ccDiclaimer=" + get_credit_card_disclaimer + "&reservAgreement=" + get_reservation_agreement + "&user_id=" + userId,
            success: function(response) {
                $('#refresh_overlay').css("display", "none");
                var responseHTML = '';
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }
                console.log(responseObj);

                if (responseObj.code == 1001) {

                    alert('Disclaimers inserted successfully');
                    getDisclaimer();
                    $('#refresh_overlay').css("display", "none");
                } else {

                    alert('Disclaimers not inserted.');
                    $('#refresh_overlay').css("display", "none");
                }
                $('#refresh_overlay').css("display", "none");
                //location.reload();

            }

        });
    }
});


/*function to get disclaimer */

function getDisclaimer() {

    $('#refresh_overlay').css("display", "block");
    var getLocalStoragevalue = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalue) == "string") {
        getLocalStoragevalue = JSON.parse(getLocalStoragevalue);
    }
    var userId = getLocalStoragevalue[0].id;

    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: "action=getDisclaimer&user_id=" + userId,
        success: function(response) {
            $('#refresh_overlay').css("display", "none");
            var responseHTML = '';
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }

            if (responseObj.code == 1002) {


            	



                $('#CCardDisclamer').val(responseObj.data[0].credit_card_disclaimer);
                $('#editor').val(responseObj.data[0].reservation_agreement);
                $('#cashPaymentDisclaimer').val(responseObj.data[0].cash_disclamer);

                $('#updateDisclaimerSetup').val('Update');
                $('#updateDisclaimerSetup').attr('updated_id', responseObj.data[0].id);
                $('#refresh_overlay').css("display", "none");
            } else {

                $('#refresh_overlay').css("display", "none");
            }
            //$('#refresh_overlay').css("display","none");

        }

    });
}


getDisclaimer();