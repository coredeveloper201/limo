
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Logout
  Author: Mylimoproject
---------------------------------------------*/

/*---------------------------------------------
    Function Name: logout function 
    Input Parameter: 
    return:redirect the page
 ---------------------------------------------*/
$('#logout').on('click', function(e){
	e.preventDefault();	
   
	window.location.href="login.html";
 });